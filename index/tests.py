from django.test import TestCase,Client
from django.urls import resolve
from .views import *
from .models import *
from selenium import webdriver
import time
class story9test(TestCase):
    def test_url_exist(self):
            response= Client().get('/')
            self.assertEqual(response.status_code,200)
    def test_using_func(self):
            found=resolve('/')
            self.assertEqual(found.func,home)
    def test_home_using_template(self):
            response = Client().get('/')
            self.assertTemplateUsed(response, 'home.html')
    def test_model_exist(self):
        Books.objects.create(id="123",image="ahaha",title="Bob Ross",authors="Bob Ross",publisher="Uganda",publish_date="1998",like="1")
        countcontent = Books.objects.all().count()
        self.assertEqual(countcontent, 1)
    def test_functional(self):
        driver=webdriver.Firefox()
        driver.get('http://localhost:8000')
        time.sleep(1)
        name=driver.find_element_by_id("search-bar")
        name.send_keys("Acta Physica Polonica")
        time.sleep(2)

        time.sleep(2)
        assert "Acta Physica Polonica" in driver.page_source

        time.sleep(5)
        #Check modals are okay
        name=driver.find_element_by_id("pasta")
        name.click()
        assert "Top 5 Liked Books" in driver.page_source
        time.sleep(1)
        driver.quit()
    def test_likes(self):
        driver=webdriver.Firefox()
        driver.get('http://localhost:8000')
        time.sleep(1)
        name=driver.find_element_by_id("search-bar")
        name.send_keys("apel")
        time.sleep(2)
        name=driver.find_element_by_xpath("//button[@onclick=\'liked(0)\']")
        name.click()
        assert "1" in driver.page_source
        time.sleep(1)
        driver.quit()