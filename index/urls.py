from django.urls import path
from .views import *

urlpatterns = [
    path('',home,name='index-home'),
    path('api/likedbooks/', book_like),
    path('api/unlikedbooks/',book_unlike),
    path('topbook/',topbook)
]
