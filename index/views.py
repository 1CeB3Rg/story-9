from django.shortcuts import render
from .models import Books
import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
def home(request):
    return render(request,'home.html')
@csrf_exempt
def book_like(request):
    data = json.loads(request.body)
    try:
        book_d = Books.objects.get(id= data['id'])
        book_d.like +=1
        book_d.save()
    except:
        try: 
            a=data['volumeInfo']['authors']
        except:
            a="Uknown"
        try: 
            p=data['volumeInfo']['publisher']
        except:
            p="Uknown"
        try: 
            d=data['volumeInfo']['publishedDate']
        except:
            d="Uknown"
        book_d = Books.objects.create(
            id = data['id'],
            image = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = a,
            publisher = p,
            publish_date = d,
            like = 1
            )
    return JsonResponse({'like': book_d.like})

@csrf_exempt
def book_unlike(request):
    data = json.loads(request.body)
    print(data)
    try:
        book_d = Books.objects.get(id= data['id'])
        if book_d.like>0:
            book_d.like -=1
        book_d.save()
    except:
        try: 
            a=data['volumeInfo']['authors']
        except:
            a="Uknown"
        try: 
            p=data['volumeInfo']['publisher']
        except:
            p="Uknown"
        try: 
            d=data['volumeInfo']['publishedDate']
        except:
            d="Uknown"
        book_d = Books.objects.create(
            id = data['id'],
            image = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors=a,
            publisher = p,
            publish_date = d,
            )
    return JsonResponse({'unlike': book_d.like})

def topbook(request):
    top_book= Books.objects.order_by('-like')
    top_list =[]
    for x in top_book:
       top_list.append({'image':x.image,'title': x.title,'authors':x.authors,'publisher':x.publisher,'published_date':x.publish_date,'like':x.like})
    return JsonResponse({'top_list':top_list})
