var b =[];
$(document).ready(function() {
    $("#search-bar").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        var xhttpr = new XMLHttpRequest();
        console.log(q);
        xhttpr.onreadystatechange = function() {
            if(this.readyState == 4 &&this.status == 200 ){
                var data = JSON.parse(this.responseText);
                b= data.items;
                $('#search-result').html('')
                var result = '';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>" +
                        "<td class='align-middle'>" +
                        "<button type ='button' onclick='liked("+i+")'> 👍</button>"+
                        "<button type ='button' onclick='unliked("+i+")'> 👎</button>"+
                        "<p id ='like"+ i +"' value='0'>0</p>"+
                        "</td>"+"</tr>"
                    }
                $('#search-result').append(result);
                }
                else{
                    
                }
            }
        xhttpr.open("GET","https://www.googleapis.com/books/v1/volumes?q=" + q,true);
        xhttpr.send();
    });
});
function liked(i){
    var data = JSON.stringify(b[i]);
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange= function(){
        if(this.readyState==4 && this.status ==200){
            var like = JSON.parse(this.responseText).like;
            $("#like"+i).html(like);
        }
    }
        xhttpr.open("POST","/api/likedbooks/");
        xhttpr.send(data);

}

function unliked(i){
    var data = JSON.stringify(b[i]);
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange= function(){
        if(this.readyState==4 && this.status ==200){
            var unliked = JSON.parse(this.responseText).unlike;
            $("#like"+i).html(unliked);
        }
    }
        xhttpr.open("POST","/api/unlikedbooks/");
        xhttpr.send(data);

}

function topbooks(){
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            var top = JSON.parse(this.responseText).top_list;
            $('.modal-body').html('');
            for (var i=0;i<5;i++){
                var author=top[i]['authors']
                
                $('.modal-body').append(
                    
                    "<p>"+(i+1)+". "+top[i]['title']+" "+
                    
                    "</p>"
                )
            }
        }
    }
    xhttpr.open("GET","/topbook/",true)
    xhttpr.send();
}